package com.example.myapplication

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_contacts.*

class ContactsFragment : Fragment() {

    companion object {
        fun newInstance() = ContactsFragment()
    }

    private lateinit var listener: FragmentListner;

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as FragmentListner
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_contacts, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener {
            listener.openAddContactFragment()
        }
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = ContactAdapter(
            Repository.contacts,
            listener::openAddContactFragment
        )


    }

    private fun onItemClicked(contact: Contact) {

    }
}