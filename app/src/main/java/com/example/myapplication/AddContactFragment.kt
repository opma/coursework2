package com.example.myapplication

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.SyncStateContract
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlinx.android.synthetic.main.fragment_add_contact.*

class AddContactFragment : Fragment() {

    companion object {
        fun newInstance() = AddContactFragment()
    }

    private var photoAddress = ""
    private val contact = Contact("", 0)

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_add_contact,
            container,
            false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        log.setOnClickListener {
            val contact = Contact(inputLogin.text.toString(), inputPass.text.toString().toInt())
            Repository.contacts.add(contact)
        }
        avatar.setOnClickListener {
            dispatchTakePictureIntent()
        }
    }

    fun takePhoto(){

    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(activity!!.packageManager)?.also {
                startActivityForResult(takePictureIntent, 1)
            }
        }
    }
    private fun makePhoto(){
        val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val photoFile = createImageFile()
        val photoURI = FileProvider.getUriForFile(
            context!!,
            "com.example.test2.fileprovider",
            photoFile
        )
        camIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
        startActivityForResult(camIntent, 1)
    }

    private fun createPhotoFile(): File {
        val stamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageName = "TEST2_" + stamp +"_"
        val storageDir = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageName, ".jpg", storageDir)
        photoAddress = image.canonicalPath
        return image
    }

    private fun setPhoto(){
        avatar.setImageURI(Uri.parse(photoAddress))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            contact.photoUri = photoAddress
            setPhoto()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {

        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            photoAddress = absolutePath
        }
    }
}