package com.example.myapplication

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_contacts.view.*

class ContactAdapter(private val list: List<Contact>, private val action: ((Contact) -> Unit)? = null) : RecyclerView.Adapter<ContactAdapter.ContactsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = ContactsViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_contacts, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class ContactsViewHolder(item: View, private val action: ((Contact) -> Unit)? = null): RecyclerView.ViewHolder(item){
        fun bind(contact: Contact){
            itemView.setOnClickListener { action?.invoke(contact) }
            itemView.name.text = contact.name
            itemView.phoneNumber.text = contact.number.toString()

        }
    }
}