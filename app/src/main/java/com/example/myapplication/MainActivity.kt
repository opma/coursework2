package com.example.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
class MainActivity : AppCompatActivity(), FragmentListner {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.root, LoginFragment()).commit()
    }

    private fun openContacts(){
        supportFragmentManager.beginTransaction()
            .replace(R.id.root, ContactsFragment.newInstance())
            .commit()
    }

    override fun changeFragment() {
        openContacts()
    }

    override fun openAddContactFragment(contact: Contact?) {
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.root, AddContactFragment.newInstance())
            .commit()
    }

}
