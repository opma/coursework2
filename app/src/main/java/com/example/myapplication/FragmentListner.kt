package com.example.myapplication

interface FragmentListner {
    fun changeFragment()
    fun openAddContactFragment(contact: Contact? = null)
}