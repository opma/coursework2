package com.example.myapplication

object Repository {
    val contacts : MutableList<Contact> = mutableListOf(
        Contact("Test", 123456789),
        Contact("Test", 12345678),
        Contact("Test", 1234567),
        Contact("Test", 123456),
        Contact("Test", 12345),
        Contact("Test", 1234),
        Contact("Test", 123),
        Contact("Test", 12),
        Contact("Test", 1)
    )
}