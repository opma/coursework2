package com.example.myapplication

data class Contact(
    val name: String,
    val number: Int,
    var photoUri: String? = null)